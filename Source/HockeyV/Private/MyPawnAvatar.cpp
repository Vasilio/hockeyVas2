// Fill out your copyright notice in the Description page of Project Settings.

#include "MyPawnAvatar.h"


// Sets default values
AMyPawnAvatar::AMyPawnAvatar()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AMyPawnAvatar::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMyPawnAvatar::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AMyPawnAvatar::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

